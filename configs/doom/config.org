#+TITLE: My doom config
#+AUTHOR: David Vogel
#+DESCRIPTION: My personal configs
#+STARTUP: showeverything

* TABLE OF CONTENTS :toc:
- [[#startup-performance][Startup performance]]
- [[#automatic-package-updates][Automatic Package Updates]]
- [[#basic-info][Basic Info]]
- [[#fonts][Fonts]]
- [[#doom-theme][Doom Theme]]
- [[#dired][Dired]]
  - [[#keybindings-to-open-dired][Keybindings To Open Dired]]
  - [[#keybindings-within-dired][Keybindings Within Dired]]
  - [[#keybindings-within-dired-with-peep-dired-mode-enabled][Keybindings Within Dired With Peep-Dired-Mode Enabled]]
- [[#org-mode][Org Mode]]
- [[#open-configs][Open Configs]]

* Startup performance
#+begin_src emacs-lisp

  ;; The default is 800 kilobytes.  Measured in bytes.
  (setq gc-cons-threshold (* 50 1000 1000))

  (defun dvd/display-startup-time ()
    (message "Emacs loaded in %s with %d garbage collections."
             (format "%.2f seconds"
                     (float-time
                       (time-subtract after-init-time before-init-time)))
             gcs-done))

  (add-hook 'emacs-startup-hook #'dvd/display-startup-time)

#+end_src
* Automatic Package Updates
The auto-package-update package helps us keep our Emacs packages up to date!  It will prompt you after a certain number of days either at startup or at a specific time of day to remind you to update your packages.

You can also use =M-x auto-package-update-now= to update right now!

#+begin_src emacs-lisp

  (use-package auto-package-update
    :custom
    (auto-package-update-interval 7)
    (auto-package-update-prompt-before-update t)
    (auto-package-update-hide-results t)
    :config
    (auto-package-update-maybe)
    (auto-package-update-at-time "09:00"))

#+end_src

* Basic Info

#+BEGIN_SRC emacs-lisp
(setq user-full-name "David Vogel"
      user-mail-address "dvogel@fastmail.com")
#+END_SRC

* Fonts

#+BEGIN_SRC emacs-lisp
(setq doom-font (font-spec :family "JetBrainsMono Nerd Font" :size 28)
      doom-variable-pitch-font (font-spec :family "Cantarell" :size 28)
      doom-big-font (font-spec :family "JetBrainsMono Nerd Font" :size 48))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
#+END_SRC

* Doom Theme

#+BEGIN_SRC emacs-lisp
(setq doom-theme 'doom-palenight)
(defvar dvd/frame-transparency '(90 . 90))
#+END_SRC
* Dired
Dired is the file manager within Emacs.  Below, I setup keybindings for image previews (peep-dired).  Doom Emacs does not use 'SPC d' for any of its keybindings, so I've chosen the format of 'SPC d' plus 'key'.

** Keybindings To Open Dired
| COMMAND    | DESCRIPTION                        | KEYBINDING |
|------------+------------------------------------+------------|
| dired      | /Open dired file manager/            | SPC d d    |
| dired-jump | /Jump to current directory in dired/ | SPC d j    |

** Keybindings Within Dired
| COMMAND             | DESCRIPTION                                 | KEYBINDING |
|---------------------+---------------------------------------------+------------|
| dired-view-file     | /View file in dired/                          | SPC d v    |
| dired-up-directory  | /Go up in directory tree/                     | h          |
| dired-find-file     | /Go down in directory tree (or open if file)/ | l          |
| dired-next-line     | Move down to next line                      | j          |
| dired-previous-line | Move up to previous line                    | k          |
| dired-mark          | Mark file at point                          | m          |
| dired-unmark        | Unmark file at point                        | u          |

** Keybindings Within Dired With Peep-Dired-Mode Enabled
| COMMAND              | DESCRIPTION                              | KEYBINDING |
|----------------------+------------------------------------------+------------|
| peep-dired           | /Toggle previews within dired/             | SPC d p    |
| peep-dired-next-file | /Move to next file in peep-dired-mode/     | j          |
| peep-dired-prev-file | /Move to previous file in peep-dired-mode/ | k          |

#+BEGIN_SRC emacs-lisp
(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))
;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!
(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))
#+END_SRC

* Org Mode
Setting the font sizes for each header level in Org mode.
#+begin_src emacs-lisp
(setq org-directory "~/org")
(setq org-ellipsis " ▾")

(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.2))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.0))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.0))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
)
#+end_src
* Open Configs
Keybindings to open files that I work with all the time using the find-file command, which is the interactive file search that opens with 'C-x C-f' in GNU Emacs or 'SPC f f' in Doom Emacs.  These keybindings use find-file non-interactively since we specify exactly what file to open.  The format I use for these bindings is 'SPC -' plus 'key' since Doom Emacs does not use these keybindings.

| PATH TO FILE                   | DESCRIPTION           | KEYBINDING |
|--------------------------------+-----------------------+------------|
| ~/Org/agenda.org               | /Edit agenda file/      | SPC - a    |
| ~/.config/doom/config.org"     | /Edit doom config.org/  | SPC - c    |
| ~/.config/doom/init.el"        | /Edit doom init.el/     | SPC - i    |
| ~/.config/doom/packages.el"    | /Edit doom packages.el/ | SPC - p    |

#+BEGIN_SRC emacs-lisp
(map! :leader
      (:prefix ("-" . "open file")
       :desc "Edit agenda file" "a" #'(lambda () (interactive) (find-file "~/Org/agenda.org"))
       :desc "Edit doom config.org" "c" #'(lambda () (interactive) (find-file "~/.config/doom/config.org"))
       :desc "Edit doom init.el" "i" #'(lambda () (interactive) (find-file "~/.config/doom/init.el"))
       :desc "Edit doom packages.el" "p" #'(lambda () (interactive) (find-file "~/.config/doom/packages.el"))))
#+END_SRC
