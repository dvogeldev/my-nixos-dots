{ config, lib, pkgs, callPackage, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "david";
  home.homeDirectory = "/home/david";

  #  services.emacs.package = pkgs.emacsUnstable;

  nixpkgs.config.allowUnfree = true;
  home.file.".config/nixpkgs/config.nix".text = ''
    { allowUnfree = true; }
  '';

  imports = [
    ./modules/zsh
    ./modules/git
    ./modules/alacritty
    ./modules/shell
    ./modules/services/dunst.nix
    ./modules/editors/neovim
    ./modules/editors/emacs.nix
  ];

  home.packages = with pkgs; [
    # Archiving
    unzip
    xz
    zip

    # Apps
    brave
    dmenu
    maestral

    # System Utils
    arandr
    binutils
    bottom
    brightnessctl
    cmake
    du-dust
    fd
    gcc
    gnumake
    gnutls
    hlint
    imagemagick
    killall
    libvterm
    nixfmt
    pass
    scrot
    skim
    slock
    tealdeer
    trayer
    unclutter
    upower
    wget
    xclip
    xdo
    xdotool
    youtube-dl

    # Emacs-related
    (aspellWithDicts (dicts: with dicts; [ en ]))
    (ripgrep.override {withPCRE2=true;})
    discount
    editorconfig-core-c
    emacs-all-the-icons-fonts
    graphviz
    html-tidy
    nodePackages.js-beautify
    nodePackages.stylelint
    proselint
    shellcheck
    sqlite
    w3m

    # Media
    git-crypt
    playerctl

    # GUI
    feh
    bibata-cursors
    lxappearance
    material-design-icons
    pamixer
    papirus-icon-theme
    pcmanfm

    (makeDesktopItem {
      name = "EXWM";
      comment = "Emacs Window Manager";
      exec = "sh /home/david/.emacs.d/exwm/start-exwm.sh";
      type = "Application";
      desktopName = "exwm";
    })
  ];

  programs.ssh.matchBlocks = {
    dv-tp = {
      hostname = "192.168.1.205";
      user = "david";
    };
    dvtp = {
      hostname = "192.168.1.205";
      user = "root";
    };
  };

  home.sessionVariables = { EDITOR = "nvim"; };

  programs.exa = { enable = true; };

  services.pasystray.enable = true;

  services.pulseeffects = {
    enable = true;
    package = pkgs.pulseeffects-legacy;
    preset = "MusicX1-Laptop";
  };

  services.keybase.enable = true;

  services.xcape = {
    enable = true;
    mapExpression = { Control_L = "Escape"; };
  };

  # FZF
  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };

  #  services.picom.enable = true;

  #programs.fish = { enable = true; };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
