{ config, lib, pkgs, ... }:

{
  xresources.properties = {
    # Palenight
    "*background" = "#292D3E";
    "*foreground" = "#d0d0d0";
    "*cursorColor" = "#BFC7D5";

    "*color0" = "#292D3E";
    "*color8" = "#434758";
    "*color1" = "#f07a78";
    "*color9" = "#ff8b92";
    "*color2" = "#c3e88d";
    "*color10" = "#ddffa7";
    "*color3"  = "#ffcb6b";
    "*color11" = "#ffe585";
    "*color4"  = "#82aaff";
    "*color12" = "#9cc4ff";
    "*color5"  = "#c792ea";
    "*color13" = "#e1acff";
    "*color6"  = "#89ddff";
    "*color14" = "#a3f7ff";
    "*color7"  = "#d0d0d0";
    "*color15" = "#ffffff";
    #Ayu-Mirage
    "/* *background" = "#1b212f */";
    "/* *foreground" = "#c3c0bb */";
    "/* *cursorColor" = "#ffcc66 */";

    "/* *color0" = "#191e2a */";
    "/* *color8" = "#686868 */";
    "/* *color1" = "#ed8274 */";
    "/* *color9" = "#f28779 */";
    "/* *color2" = "#a6cc70 */";
    "/* *color10" = "#bae67e */";
    "/* *color3"  = "#fad07b */";
    "/* *color11" = "#ffd580 */";
    "/* *color4"  = "#6dcbfa */";
    "/* *color12" = "#73d0ff */";
    "/* *color5"  = "#cfbafa */";
    "/* *color13" = "#d4bfff */";
    "/* *color6"  = "#90e1c6 */";
    "/* *color14" = "#95e6cb */";
    "/* *color7"  = "#c7c7c7 */";
    "/* *color15" = "#ffffff */";
    "/* *selection_foreground" = "#212733 */";
  };
}
