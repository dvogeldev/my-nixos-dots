{
    # Utilities
    ".." = "cd ..";
    "..." = "cd ../..";
    "...." = "cd ../../..";
    "....." = "cd ../../../..";
    cl = "clear";
    cla = "clear && ls -la";
    cll = "clear && exa -lh --group-directories-first";
    cls = "clear && ls";
    # Home-manager
    hme = "home-manager edit"; #TODO this is a window manager function
    hms = "$HOME/projects/nix-config/scripts/utilities/home-update.sh";
    # Nixos
    huc = "nix-channel --update";
    ncl = "doas nix-channel --list";
    nrs = "doas nixos-rebuild switch";
    nrb = "doas nixos-rebuild boot";
    nu = "huc & nrs";
    ncu = "doas nix-channel --update";
    nup = "$HOME/projects/nix-config/scripts/utilities/system-update-pc.sh";
    nut = "$HOME/projects/nix-config/scripts/utilities/system-update-tp.sh";
    # Program shortcuts
    cat = "bat -pp";
    l = "exa --group-directories-first";
    la = "exa -lah --group-directories-first";
    ll = "exa -lg --group-directories-first";
    lm = "exa -lgs modified --group-directories-first";
    ls = "exa -lag --group-directories-first";
    lt = "exa -T";
    v = "nvim";
    # Git
    gall = "git add .";
    gcom = "git commit -m";
    gd = "git diff";
    gf = "git fetch";
    gl = "git log";
    gph = "git push";
    gpl = "git pull";
    gs = "git status";
    # Pass
    pe = "pass edit";
    pi = "pass insert";
    pl = "pass list";
    pp = "pass git push";
    pr = "pass rm";
    pu = "pass git pull";
    # Configs
    sudo = "doas";
    # Jumps
    prj = "cd $HOME/projects";
    nc = "cd $HOME/projects/nix-config";
    oc = "cd $HOME/projects/org-config";
}
