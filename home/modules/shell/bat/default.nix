{ pkgs, ... }:

{

  programs.bat.enable = true;
  programs.bat.themes = {
   dracula = builtins.readFile (pkgs.fetchFromGitHub {
     owner = "dracula";
     repo = "sublime"; # Bat uses sublime syntax for its themes
     rev = "26c57ec282abcaa76e57e055f38432bd827ac34e";
     sha256 = "019hfl4zbn4vm4154hh3bwk6hm7bdxbr1hdww83nabxwjn99ndhv";
   } + "/Dracula.tmTheme");
    ayu-mirage = builtins.readFile ./themes/ayu-mirage.tmTheme;
    palenight = builtins.readFile ./themes/palenight.tmTheme;
  };
  programs.bat.config.italic-text = "always";
  programs.bat.config.theme = "palenight";

}
