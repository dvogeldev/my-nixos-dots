{ config, pkgs, ... }:

{
  imports = [
    ./nnn.nix # NNN file manager with NerdFonts
    ./xresources.nix
    ./bat # Bat with Palenight Theme
  ];
}
