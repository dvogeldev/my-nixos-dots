{ config, lib, pkgs, ... }:

let
  custom-plugins = pkgs.callPackage ./custom-plugins.nix {
    inherit (pkgs.vimUtils) buildVimPlugin;
  };

  plugins = pkgs.vimPlugins // custom-plugins;

  overriddenPlugins = with pkgs; [];

  myVimPlugins = with plugins; [
    vim-surround    #quickly edit surroundings (brackets, html tags, etc)
    nerdtree                # tree explorer
    vim-airline             # bottom status bar
    vim-fish                # fish shell highlighting
    vim-css-color           # preview css colors
    vim-nix                 # nix support (highlighting, etc)
    haskell-vim             # haskell support
    palenight-vim           # palenight theme for vim
    nord-vim
    rainbow_parentheses-vim # for nested parentheses
    vim-devicons            # dev icons shown in the tree explorer
    ayu-vim
  ] ++ overriddenPlugins;

  baseConfig  = builtins.readFile ./config.vim;
  pluginsConfig = builtins.readFile ./plugins.vim;
  vimConfig     = baseConfig + pluginsConfig;

in

{
  programs.neovim = {
    enable        = true;
    extraConfig   = vimConfig;
    plugins       = myVimPlugins;
    viAlias       = true;
    vimAlias      = true;
    vimdiffAlias  = true;
  };
}
