# { pkgs, fetchFromGitHub }:
#
# let
#   buildVimPlugin = pkgs.vimUtils.buildVimPluginFrom2Nix;
# in
#
# {
#   vim-ripgrep = buildVimPlugin {
#     name = "vim-ripgrep";
#     src = fetchFromGitHub {
#       owner = "jremmen";
#       repo = "vim-ripgrep";
#       rev = "ec87af6b69387abb3c4449ce8c4040d2d00d745e";
#       sha = "sFp57KGnMu3a7pTNPx3vNfuPhMhJqc22tHWBTF02xa8=";
#     };
#   };
#   neovim-ayu = buildVimPlugin {
#     name = "neovim-ayu";
#     src = fetchFromGitHub {
#       owner = "Shatur";
#       repo = "neovim-ayu";
#       rev = "4d43b6037fdaa1752a1aca01c08868109afe6c9e";
#       sha = "2LqOFxBwbnG38UfrAb4eA4keE1akfCWbAfRBkkPNTmg=";
#     };
#   };
#
# }
{ config, pkgs, ...}:

let
  neovim-ayu = pkgs.vimUtils.buildVimPlugin {
    name = "neovim-ayu";
    src = pkgs.fetchFromGitHub {
      owner = "Shatur";
      repo = "neovim-ayu";
      rev = "4d43b6037fdaa1752a1aca01c08868109afe6c9e";
      sha = "2LqOFxBwbnG38UfrAb4eA4keE1akfCWbAfRBkkPNTmg=";
     };
  };
  vim-ripgrep = pkgs.vimUtils.buildVimPlugin {
    name = "vim-ripgrep";
    src = pkgs.fetchFromGitHub {
      owner = "jremmen";
      repo = "vim-ripgrep";
      rev = "ec87af6b69387abb3c4449ce8c4040d2d00d745e";
      sha = "sFp57KGnMu3a7pTNPx3vNfuPhMhJqc22tHWBTF02xa8=";
    };
  };

in
{
  environment.systemPackages = [
    (
      pkgs.neovim.override {
        configure = {
          packages.myPlugins = with pkgs.vimPlugins; {
          start = [
            neovim-ayu
            vim-ripgrep
          ];
          opt = [];
          };
          # ...
        };
      }
    )
  ];
}
