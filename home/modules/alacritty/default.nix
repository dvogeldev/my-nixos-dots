{ fontSize, pkgs, ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {
      env.TERM = "xterm-256color";
      window = {
        dimensions = {
          columns = 100;
          lines = 40;
        };
        padding = {
          x = 12;
          y = 0;
        };
        dynamic_padding = false;
        class = {
          instance = "Alacritty";
          general = "Alacritty";
        };
        decorations = "none";
        startup_mode = "Windowed";
      };
      scrolling = {
        history = 10000;
        multiplier = 3;
        tabspaces = 2;
      };
      font = {
        family = "JetBrainsMono Nerd Font";
        size = 12;
      };
      draw_bold_text_with_bright_colors = true;
      # Palenight theme
       colors = {
         primary = {
           background = "#292D3E";
           foreground = "#BFC7D5";
         };
         cursor = {
           text = "#49B9C7";
           cursor = "#F6F6F6";
         };
         selection = {
           text = "#FFFFFF";
           background = "#48B9C7";
         };
         normal = {
           black = "#292d3e";
           red = "#F07178";
           green = "#C3E88D";
           yellow = "#FFCB6B";
           blue = "#82AAFF";
           magenta = "#C792EA";
           cyan = "#60ADEC";
           white = "#ABB2BF";
         };
         bright = {
           black = "#959DCB";
           red = "#F07178";
           green = "#C3E88D";
           yellow = "#FF5572";
           blue = "#82AAFF";
           magenta = "#FFCB6B";
           cyan = "#676E95";
           white = "#FFFEFE";
         };

      # colors = {
      #   primary = {
      #     background = "#1f2430";
      #     foreground = "#cbccc6";
      #     bright_foreground = "#f28779";
      #   };

      #   # Normal colors
      #   normal = {
      #     black = "#212733";
      #     red = "#f08778";
      #     green = "#53bf97";
      #     yellow = "#fdcc60";
      #     blue = "#60b8d6";
      #     magenta = "#ec7171";
      #     cyan = "#98e6ca";
      #     white = "#fafafa";
      #   };

      #   # Brightened
      #   bright = {
      #     black = "#686868";
      #     red = "#f58c7d";
      #     green = "#58c49c";
      #     yellow = "#ffd165";
      #     blue = "#65bddb";
      #     magenta = "#f17676";
      #     cyan = "#9debcf";
      #     white = "#ffffff";
      #   };

        background_opacity = 0;
        live_config_reload = true;
        selection.save_to_clipboard = true;
        shell.program = "${pkgs.zsh}/bin/zsh";
      };
      # Added some bogus text
      keybindings = [
        { key = "k"; mods = "Shift|Alt"; action = "ScrollPageUp"; }
        { key = "j"; mods = "Shift|Alt"; action = "ScrollPageDown"; mode = "~Alt"; }
        { key = "C"; mods = "Control|Shift"; action = "Copy"; }
        { key = "V"; mods = "Control|Shift"; action = "Paste"; }
        { key = "Up"; mods = "Control|Shift"; action = "ScrollPageUp"; }
        { key = "Down"; mods = "Control|Shift"; action = "ScrollPageDown"; }
        { key = "Equals"; mods = "Control"; action = "IncreaseFontSize"; }
        { key = "Plus"; mods = "Control"; action = "IncreaseFontSize"; }
        { key = "Minus"; mods = "Control"; action = "DecreaseFontSize"; }
        { key = "Key0"; mods = "Control"; action = "ResetFontSize"; }
      ];
    };
  };
}
