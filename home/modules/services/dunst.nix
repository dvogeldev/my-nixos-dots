# This file is generated from modules/README.org
{ config, ... }:

{

  services.dunst.enable = true;

  services.dunst.settings = {
    global = {
      monitor = 0;
      follow = "keyboard";
      geometry = "960x5-0+30";
      indicate_hidden = "yes";
      shrink = "no";
      transparency = 20;
      notification_height = 0;
      separator_height = 2;
      padding = 0;
      horizontal_padding = 8;
      frame_width = 3;
      frame_color = "#959DCB";

      separator_color = "frame";

      # Sort messages by urgency.
      sort = "yes";
      idle_threshold = 120;
      font = "JetBrainsMono Nerd Font 12";
      line_height = 0;
      markup = "full";

      format = "<b>%s</b>\n%b";

      alignment = "left";
      show_age_threshold = 60;
      word_wrap = "yes";
      ellipsize = "middle";
      ignore_newline = "no";
      stack_duplicates = "true";
      hide_duplicate_count = "true";
      show_indicators = "yes";
      icon_position = "left";
      max_icon_size = 40;
      sticky_history = "yes";
      history_length = 20;
      title = "Dunst";
      class = "Dunst";
      startup_notification = "false";
      force_xinerama = "false";
    };


    urgency_low = {
      background = "#444267";
      foreground = "#676E95";
      timeout = "5";
    };

    urgency_normal = {
      background = "#32374D";
      foreground = "#959DCB";
      timeout = "5";
    };

    urgency_critical = {
      background = "#F07178";
      foreground = "#959DCB";
      timeout = "5";
    };

    shortcuts = {
      close = "ctrl+space";
      close_all = "ctrl+shift+space";
      history = "ctrl+grave";
      context = "ctrl+shift+period";
    };
  };

}
