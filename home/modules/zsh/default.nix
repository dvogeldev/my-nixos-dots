{ config, lib, pkgs, ... }:

{
  imports = [
    ./plugins.nix
  ];

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    shellAliases = (import ../shell/aliases.nix);
    autocd = true;
    history = {
      extended = true;
      ignoreDups = true;
      share = true;
      save = 100000;
      path = "~/.config/zsh/zsh_history";
    };
    dotDir = ".config/zsh";
    sessionVariables = config.home.sessionVariables;
    # Autocompletions
  };
    
  # Starship prompt
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };
}   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
