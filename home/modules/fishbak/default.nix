{ config, pkgs, lib, ... }:

let
  fzfConfig = ''
    set -x FZF_DEFAULT_OPTS "--preview='bat {} --color=always'" \n
    set -x SKIM_DEFAULT_COMMAND "rg --files || fd || find ."
  '';

  customPlugins = pkg.callPackage ./plugins.nix {};

  fishConfig = ''
    bind \t accept-autosuggestions
    fish_vi_key_bindings
    set fish_greeting
  '' + fzfConfig;

in

{

  programs.fish = {
    enable = true;
    plugins = [ customPlugins ];
    shellAliases = (import ../shell/aliases.nix);
    shellInit = fishConfig;
  };


  # Starship prompt
  programs.starship = {
    enable = true;
    enableFishIntegration = true;
  };
}
