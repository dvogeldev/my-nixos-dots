{ configs, pkgs, ... }:

{
  programs.fish.plugins = [

    {
      name = "z";
      src = pkgs.fetchFromGitHub {
       owner = "jethrokuan";
       repo = "z";
       rev = "ccb0ac58bc09841eaa2a54bf2aa7e9fb871d0e3f";
       sha256 = "l5y4cFwO1Lqe/tFNO87F/zs8zA1JXLeYCIKuX6el5hc=";
       fetchSubmodules = false;
      };
    }

    {
      name = "fish-ssh-agent";
      src = pkgs.fetchFromGitHub {
        owner = "danhper";
        repo = "fish-ssh-agent";
        rev = "fd70a2afdd03caf9bf609746bf6b993b9e83be57";
        sha256 = "e94Sd1GSUAxwLVVo5yR6msq0jZLOn2m+JZJ6mvwQdLs=";
        fetchSubmodules = false;
      };
    }

    {
      name = "pisces";
      src = pkgs.fetchFromGitHub {
       owner = "laughedelic";
       repo = "pisces";
       rev = "e45e0869855d089ba1e628b6248434b2dfa709c4";
       sha256 = "Oou2IeNNAqR00ZT3bss/DbhrJjGeMsn9dBBYhgdafBw=";
       fetchSubmodules = false;
      };
    }

    {
      name = "bang-bang";
      src = pkgs.fetchFromGitHub {
       owner = "oh-my-fish";
       repo = "plugin-bang-bang";
       rev = "f969c618301163273d0a03d002614d9a81952c1e";
       sha256 = "1r3d4wgdylnc857j08lbdscqbm9lxbm1wqzbkqz1jf8bgq2rvk03";
       fetchSubmodules = false;
      };
    }

  ];
}
