{ config, pkgs, ... }:

{

  programs.zsh.zplug = {
    enable = true;
    plugins = [
      {
        name = "zsh-users/zsh-syntax-highlighting";
        tags = [ "defer:2" ];
      }
      {
        name = "jeffreytse/zsh-vi-mode";
      }
    ];
  };

}
