{ config, lib, pkgs, ... }:

  let
    gitConfig = {
      core = {
        editor = "nvim";
        pager = "diff-so-fancy | less --tabs=4 -RFX";
      };
      init.defaultBranch = "main";
      merge.tool = "vimdiff";
      mergetool = {
        cmd = "nvim -f -c \"Gvdiffsplit!\" \"$MERGED\"";
        prompt = false;
      };
      pull.rebase = false;
    };
  in
{
  programs.git = {
    enable = true;
    extraConfig = gitConfig;
    package = pkgs.gitAndTools.gitFull;
    ignores = [
      "*.direnv"
      "*.envrc"
      "*.jvmopts"
    ];
    signing = {
      key = "AA433486035A7493944145BE286070A95F700555";
      signByDefault = true;
    };
    userEmail = "dvogel@fastmail.com";
    userName = "David Vogel";
  };

}
