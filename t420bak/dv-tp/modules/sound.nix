{ config, libs, pkgs, ...  }:

{

environment.systemPackages = [ pkgs.bluez ];
security.rtkit.enable = true;
services.pipewire = {
  enable = true;
  alsa.enable = true;
  pulse.enable = true;
  media-session.config.bluez-monitor.rules = [
    {
      matches = [{ "device.name" = "~bluez_card.*"; }];
      actions = {
        "update-props" = {
	  "bluez5.reconnect-profiles" = [ "hfp_hf" "hsp_hs" "a2db_sink" ];
	  "bluez5.msbc-support" = true;
	  "bluez5.sbc-xq-support" = true;
        };

      };
    }
    {
      matches = [
        { "node.name" = "~bluez_input.*"; }
	{ "node.name" = "~bluez_output.*"; }
      ];
      actions = {
        "node.pause-on-idle" = false;
      };
    }
  ];
};

}
