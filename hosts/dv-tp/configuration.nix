# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./modules/sound.nix
    ];

  # Microcode
  hardware.cpu.intel.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    # ZFS
    supportedFilesystems = [ "zfs" ];
    zfs.requestEncryptionCredentials = true;
  };

  services.zfs.autoScrub.enable = true;
  
  services.keybase.enable = true;

  # Zram swap
  zramSwap.enable = true;

  # Networking
  networking = {
    hostName = "dv-tp";
    networkmanager.enable = true;
    hostId = "97534807";
    useDHCP = false;
    interfaces.enp0s25.useDHCP = true;
    interfaces.wlp3s0.useDHCP = true;
  };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Hack";
    keyMap = "us";
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    dpi = 120;
    xkbOptions = "ctrl:nocaps";
    #videoDrivers = [ "intel" ];
    #displayManager.startx.enable = true;
    displayManager.lightdm.enable = true;
    windowManager.spectrwm.enable = true;
  };
  services.xserver.videoDrivers = ["intel"];


  # Enable CUPS to print documents.
  # services.printing.enable = true;

#  # Enable sound.
#  sound.enable = true;
#  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput = {
    enable = true;
    touchpad.disableWhileTyping = true;
  };

  # Enable bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.david = {
    initialPassword = "pass";
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" ]; # Enable ‘sudo’ for the user.
  };

  # Trusted users
  nix.trustedUsers = [ "root" "david" ];

  # Security
  security.doas = {
    enable = true;
    extraRules = [
      {
        groups = [ "wheel" ];
	keepEnv = true;
	noPass = true;
      }
    ];
  };
  # Disable sudo
  security.sudo.enable = false;

  # Allow unfree
  nixpkgs.config = {
    allowUnfree = true;
    joypixels.acceptLicense = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    alacritty
    brave
    #cachix
    dmenu
    fontconfig
    hack-font
    haskellPackages.xmobar
    libcpuid
    neofetch
    neovim
    pinentry-gtk2
    pulseaudio-ctl
    polybar
    slock
    wget
    xcape
  ];

 # Fonts
 fonts.fonts = with pkgs; [
   #cantarell-fonts
   font-awesome
   joypixels
   hack-font
   (nerdfonts.override { fonts = [ "JetBrainsMono" ];})
   roboto
 ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "gtk2";
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # SSH Known hosts
  programs.ssh.extraConfig = ''
    # Hosts with dash is root login where allowed
    Host *
      IdentityFile ~/.ssh/id_rsa
    
    Host dv-tp
    	HostName 192.168.1.205
    	User david
        IdentityFile ~/.ssh/id_ed25519
    Host dvtp
    	HostName 192.168.1.205
    	User root
        IdentityFile ~/.ssh/id_ed25519
    Host dv-pc
    	HostName 192.168.1.210
    	User david
        IdentityFile ~/.ssh/id_ed25519
    Host dvpc
    	HostName 192.168.1.210
    	User root
        IdentityFile ~/.ssh/id_ed25519
    Host dv-mbp
    	HostName 192.168.1.200
    	User dvogel
        IdentityFile ~/.ssh/id_ed25519
    Host gatmail
    	HostName 155.138.194.162
    	User root
    	IdentityFile ~/.ssh/id_ed25519
    Host gitlab
    	HostName https://gitlab.com
    	User dvogeldev
    	IdentityFile ~/.ssh/id_ed25519
    '';

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

