{ config, lib, pkgs, ... }:

{

  # X11
  services.xserver = {
    enable = true;
    autorun = true;
    layout = "us";
    xkbOptions = "ctrl:nocaps";
    autoRepeatDelay = 200;
    autoRepeatInterval = 25;
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    # desktopManager.xfce.enable = true;
  };
  environment.systemPackages = with pkgs; [
    xorg.xwininfo
  ];
  services.xserver.displayManager = {
    defaultSession = "none+xmonad";
    lightdm = {
      enable = true;
       greeters.mini = {
         enable = true;
         user = "david";
         extraConfig = ''
           [greeter]
           show-password-label = false
           [greeter-theme]
           text-color = "#d0d0d0"
           error-color = "#f07a78"
           window-color = "#292d3e"
           border-color = "#292D3E"
           password-color = "#c789ea"
           password-background-color = "#434758"
           password-border-color = "#434758"
           border-width = 2px
           background-image = "../wallpapers/wallpaper2.png"
         '';
       };
    };
  };
}
