{ config, lib, pkgs, ... }:

{
  # Select internationalisation properties.
  console = {
    font = "Lat2-Terminus16";
    earlySetup = true;
    keyMap = "us";
    colors = [
      "292D3E"
      "434758"
      "f07a78"
      "ff8b92"
      "c3e88d"
      "ddffa7"
      "ffcb6b"
      "ffe585"
      "82aaff"
      "9cc4ff"
      "c792ea"
      "e1acff"
      "89ddff"
      "a3f7ff"
      "d0d0d0"
      "ffffff"
    ];
  };
}
