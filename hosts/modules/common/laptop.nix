{ config, lib, pkgs, ... }:

{
  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput = {
    enable = true;
    touchpad.disableWhileTyping = true;
  };

  # System sensors
  services.thermald.enable = true;
  # services.thinkfan.sensors = true;
}
