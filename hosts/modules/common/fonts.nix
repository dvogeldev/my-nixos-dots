{ config, lib, pkgs, ... }:

{
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    cantarell-fonts
    roboto
    libertine
    joypixels
    font-awesome
  ];
  nixpkgs.config.joypixels.acceptLicense = true;

  fonts.fontconfig = {
    defaultFonts = {
      monospace = [ "JetBrainsMono Nerd Font" ];
      serif = [ "Libertine" ];
      sansSerif = [ "Cantarell" ];
      emoji = [ "Joypixels" ];
    };
  };
  fonts.fontconfig.enable = true;
  fonts.fontconfig.allowBitmaps = true;
  fonts.fontconfig.useEmbeddedBitmaps = true;
  fonts.fontDir.enable = true;
  fonts.enableGhostscriptFonts = true;
}
