{ config, lib, pkgs, ... }:

{

  environment.systemPackages = with pkgs; [
    brave
    cabal-install
    cachix
    bibata-cursors-translucent
    #font-awesome
    gcc
    haskellPackages.xmobar
    harfbuzz
    lm_sensors
    xorg.xbacklight
    libcpuid
    neovim
    pinentry-gtk2
    pulseaudio-ctl
    wget
    xcape
    zsh-powerlevel10k
  ];

  nixpkgs.config.allowUnfree = true;
}
