{ config, pkgs, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    forwardX11 = true;
    permitRootLogin = "prohibit-password";
    passwordAuthentication = false;
  };
  programs.ssh.extraConfig = ''
    Host gitlab.com
      UpdateHostKeys no
    Host *
      IdentityFile ~/.ssh/id_ed25519
    Host dv-tp
      Hostname 192.168.1.205
      User david
    Host dv-pc
      Hostname 192.168.1.210
      User david
    Host gatmail
      Hostname 155.138.194.162
      User root
      IdentityFile ~/.ssh/id_ed25519
  '';
}
