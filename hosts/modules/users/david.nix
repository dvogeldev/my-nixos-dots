{ config, lib, pkgs, ... }:
{
  users.users.david = {
    isNormalUser = true;
    initialPassword = "pass";
    extraGroups = [ "wheel" "audio" "video" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };
  time.timeZone = "America/Detroit";
}
