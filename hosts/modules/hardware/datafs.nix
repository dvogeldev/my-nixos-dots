{ config, lib, pkgs, ... }:

{

  fileSystems."/data" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@" ];
    };

  fileSystems."/data/dots" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@dots" ];
    };

  fileSystems."/data/pics" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@pics" ];
    };

  fileSystems."/data/videos" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@videos" ];
    };

  fileSystems."/archive" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@archive" ];
    };

  fileSystems."/data/projects" =
    { device = "/dev/disk/by-uuid/228658b0-3460-4e0d-87d5-f73a4a7a9b12";
      fsType = "btrfs";
      options = [ "subvol=@projects" ];
    };
}
