# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./modules/fonts.nix
    ];

  # Make ready for nix flakes
  nix.package = pkgs.nixFlakes;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Intel microcode
  hardware.cpu.amd.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # ZFS
  boot = {
    supportedFilesystems = [ "zfs"];
    zfs.requestEncryptionCredentials = true;
  };
  services.zfs.autoScrub.enable = true;

  # zram
  zramSwap.enable = true;

  # Networking
  networking = {
    hostName = "dv-pc";
    networkmanager.enable = true;
    hostId = "1fd01a88";
  };

  # Set your time zone.
  time.timeZone = "America/Detroit";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp27s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Roboto";
    keyMap = "us";
  };

  # X11
  services.xserver = {
    enable = true;
    layout = "us";
    dpi = 192;
    xkbOptions = "ctrl:nocaps";
    videoDrivers = [ "nvidia" ];
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
  };

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [ pkgs.hplip ];
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput = {
    enable = true;
    touchpad.disableWhileTyping = true;
  };

  # System sensors
  services.thermald.enable = true;
  # services.thinkfan.sensors = true;

  # Security
#   security.doas = {
#     enable = true;
#     extraRules = [
#       {
#         groups = [ "wheel" ];
# 	keepEnv = true;
# 	noPass = true;
#       }
#     ];
#   };
  # Disable sudo
#  security.sudo.enable = false;

  # Trusted users
  nix.trustedUsers = [ "root" "david" ];

  nix = {
    trustedBinaryCaches = [
      https://nix-community.cachix.org
      https://cache.nixos.org
    ];
    binaryCachePublicKeys = [
      nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=
      cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=
    ];
  };

  # Default shell
  users.defaultUserShell = pkgs.zsh;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.david = {
    isNormalUser = true;
    initialPassword = "pass";
    extraGroups = [ "wheel" "audio" "video" ]; # Enable ‘sudo’ for the user.
    useDefaultShell = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    brave
    cachix
    bibata-cursors-translucent
    #font-awesome
    gcc
    haskellPackages.xmobar
    lm_sensors
    xorg.xbacklight
    libcpuid
    neovim
    pinentry-gtk2
    pulseaudio-ctl
    wget
    xcape
    zsh-powerlevel10k
    #hack-font
    #roboto
    #(nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    #cantarell-fonts
  ];

  nixpkgs.config = {
    allowUnfree = true;
    joypixels.acceptLicense = true;
  };

  ## Fonts
  #fonts.fonts = with pkgs; [
  #  cantarell-fonts
  #  hack-font
  #];

  # Environment variables
  environment.variables = {
    EDITOR = "nvim";
  };

  programs.mosh.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "gtk2";
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # SSH known hosts
  programs.ssh.knownHosts = {
    dv-pc = {
      hostNames = [ "dv-pc" "192.168.1.210"];
      publicKeyFile = /home/david/.ssh/id_ed25519.pub;
    };
    dv-tp = {
      hostNames = [ "dv-tp" "192.168.1.205"];
      publicKeyFile = /home/david/.ssh/id_ed25519.pub;
    };
  };

  # Auto upgrades
  system.autoUpgrade.enable = true;

  # Remove old generations
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 8d";
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}
