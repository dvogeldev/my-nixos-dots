{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ../modules/common/console.nix
    ../modules/common/doas.nix
    ../modules/common/fonts.nix
    ../modules/common/globallocale.nix
    ../modules/common/nix.nix
    ../modules/users/david.nix
    ../modules/common/systemPackages.nix
    ../modules/common/xserver.nix
    ../modules/common/xmonad.nix
    ../modules/hardware/datafs.nix    # Adds BTRFS Raid
    ../modules/hardware/printer.nix   # HP Deskjet
    ../modules/hardware/sound.nix     # Pipewire
    ../modules/services/openssh.nix
  ];

  # Intel microcode
  hardware.cpu.amd.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # ZFS
  boot = {
    supportedFilesystems = [ "zfs" "btrfs" ];
    zfs.requestEncryptionCredentials = true;
  };
  services.zfs.autoScrub.enable = true;

  # zram
  zramSwap.enable = true;

  # Networking
  networking = {
    hostName = "dv-pc";
    networkmanager.enable = true;
    hostId = "1fd01a88";
    useDHCP = false;
    interfaces.enp27s0.useDHCP = true;
  };

  # Enable numlock for desktop
  environment = {
    systemPackages = with pkgs; [ numlockx ];
  };


  # Environment variables
#  environment.variables = (import ../modules/common/globalvars.nix);
  environment.variables = {
    EDITOR = "nvim";
    COLORTERM = "truecolor";
  };
  environment.shellAliases = {
    sudo = "doas";
    cl = "clear";
  };

  programs.mosh.enable = true;
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:
  services.xserver = {
    dpi = 192;
    videoDrivers = [ "nvidia" ];
  };

  # Auto upxgrades
  system.autoUpgrade.enable = true;

  # Remove old generations
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 8d";
  };

  system.stateVersion = "21.05"; # Did you read the comment?

}
