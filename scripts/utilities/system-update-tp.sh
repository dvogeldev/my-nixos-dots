#!/bin/sh

set -euo pipefail

origPath=`pwd`

cd "$HOME/projects/nix-config/hosts/dv-tp"
doas nixos-rebuild test -I nixos-config=./configuration.nix

cd $origPath
