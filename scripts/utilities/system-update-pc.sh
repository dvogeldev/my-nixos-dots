#!/bin/sh

set -euo pipefail

origPath=`pwd`

cd "$HOME/projects/nix-config/hosts/dv-pc"
doas nixos-rebuild switch -I nixos-config=./configuration.nix

cd $origPath
