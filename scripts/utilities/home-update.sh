#!/bin/sh

set -euo pipefail

origPath=`pwd`

cd "$HOME/projects/nix-config/home"
home-manager switch -f ./home.nix

cd $origPath
