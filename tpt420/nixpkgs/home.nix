{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "david";
  home.homeDirectory = "/home/david";


  services.emacs.package = pkgs.emacsGcc;
  services.emacs.enable = true;

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
    }))
  ];

  nixpkgs.config.joypixels.acceptLicense = true;
  nixpkgs.config.allowUnfree = true;
  home.file.".config/nixpkgs/config.nix".text = ''
    { allowUnfree = true; }
  '';

  imports = [
    ./modules/zsh
    ./modules/git
    ./modules/alacritty
    ./modules/shell/xresources.nix
  ];

  home.packages = with pkgs; [
    bc  # Needed for polybar/temp script
    arandr
    brave
    brightnessctl
    cantarell-fonts
    dmenu
    fd
    feh
    font-awesome
    fzf
    joypixels
    killall
    lxappearance
    material-design-icons
    nyxt
    gitAndTools.diff-so-fancy
    pamixer
    papirus-icon-theme
    pcmanfm
    playerctl
    polybar
    rofi
    rclone
    ripgrep
    scrot
    slock
    sqlite
    tealdeer
    unclutter
    unzip
    upower
    w3m
    wget
    xclip
    xdo
    youtube-dl

    (makeDesktopItem {
      name = "EXWM";
      comment = "Emacs Window Manager";
      exec = "sh /home/david/.emacs.d/exwm/start-exwm.sh";
      type = "Application";
      desktopName = "exwm";
    })
  ];

  home.sessionVariables = {
     EDITOR = "nvim";
   };

  programs.exa = {
    enable = true;
  };

  programs.emacs = {
    enable = true;
    package = pkgs.emacsGcc;
  };

  services.pasystray.enable = true;

  services.pulseeffects = {
    enable = true;
    package = pkgs.pulseeffects-legacy;
    preset = "MusicX1-Laptop";
  };

  services.xcape = {
    enable = true;
    mapExpression = { Control_L = "Escape";};
  };

  services.dunst = {
    enable = true;
  };

  services.picom.enable = true;

  programs.fish = {
    enable = true;
  };


  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
