{ config, lib, pkgs, ... }:

{
  xresources.properties = {
    # Palenight
    "*background" = "#292D3E";
    "*foreground" = "#d0d0d0";
    "*cursorColor" = "#BFC7D5";

    "*color0" = "#292D3E";
    "*color8" = "#434758";
    "*color1" = "#f07a78";
    "*color9" = "#ff8b92";
    "*color2" = "#c3e88d";
    "*color10" = "#ddffa7";
    "*color3"  = "#ffcb6b";
    "*color11" = "#ffe585";
    "*color4"  = "#82aaff";
    "*color12" = "#9cc4ff";
    "*color5"  = "#c792ea";
    "*color13" = "#e1acff";
    "*color6"  = "#89ddff";
    "*color14" = "#a3f7ff";
    "*color7"  = "#d0d0d0";
    "*color15" = "#ffffff";
  };
}
