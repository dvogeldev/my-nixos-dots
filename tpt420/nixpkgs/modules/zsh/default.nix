{ config, lib, pkgs, ... }:

{
  imports = [
    ./plugins.nix
  ];

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    shellAliases = {
      # Utilities
      ".." = "cd ..";
      "..." = "cd ../..";
      "...." = "cd ../../..";
      "....." = "cd ../../../..";
      cl = "clear";
      cla = "clear && ls -la";
      cll = "clear && exa -lh --group-directories-first";
      cls = "clear && ls";
      # Home-manager
      hme = "home-manager edit";
      hms = "home-manager switch";
      # Nixos
      huc = "nix-channel --update";
      ncl = "sudo nix-channel --list";
      ncu = "sudo nix-channel --update";
      # Program shortcuts
      cat = "bat -pp";
      l = "exa --group-directories-first";
      ll = "exa -lg --group-directories-first";
      lm = "exa -lgs modified --group-directories-first";
      ls = "exa -lag --group-directories-first";
      v = "nvim";
      # Git
      gall = "git add .";
      gcom = "git commit -m";
      gd = "git diff";
      gf = "git fetch";
      gl = "git log";
      gph = "git push";
      gpl = "git pull";
      gs = "git status";
      # Pass
      pe = "pass edit";
      pi = "pass insert";
      pl = "pass list";
      pp = "pass git push";
      pr = "pass rm";
      pu = "pass git pull";
      # Configs
    };

    autocd = true;
    history = {
      extended = true;
      ignoreDups = true;
      share = true;
      save = 100000;
      path = ".config/zsh/zsh_history";
    };
    dotDir = ".config/zsh";
    sessionVariables = config.home.sessionVariables;
  };
  # Starship prompt
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };
}
