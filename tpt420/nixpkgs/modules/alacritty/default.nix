{ fontSize, pkgs, ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        dimensions = {
          columns = 100;
          lines = 40;
        };
        padding = {
          x = 6;
          y = 6;
        };
        class = {
          instance = "Alacritty";
          general = "Alacritty";
        };
      };
      scrolling = {
        history = 5000;
        multiplier = 3;
        tabspaces = 2;
      };
      font = {
        family = "JetBrainsMono Nerd Font";
        size = 18;
      };
      draw_bold_text_with_bright_colors = true;
      colors = {
        primary = {
          background = "#292D3E";
          foreground = "#BFC7D5";
        };
        cursor = {
          text = "#49B9C7";
          cursor = "#F6F6F6";
        };
        selection = {
          text = "#FFFFFF";
          background = "#48B9C7";
        };
        normal = {
          black = "#292d3e";
          red = "#F07178";
          green = "#C3E88D";
          yellow = "#FFCB6B";
          blue = "#82AAFF";
          magenta = "#C792EA";
          cyan = "#60ADEC";
          white = "#ABB2BF";
        };
        bright = {
          black =  "#959DCB";
          red = "#F07178";
          green = "#C3E88D";
          yellow = "#FF5572";
          blue = "#82AAFF";
          magenta = "#FFCB6B";
          cyan = "#676E95";
          white = "#FFFEFE";
        };
        background_opacity = 1.0;
        live_config_reload = true;
        selection.save_to_clipboard = true;
        shell.program = "${pkgs.zsh}/bin/zsh";
      };
    };
  };
}
